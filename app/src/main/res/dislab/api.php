<?php 
    require_once 'Db.php';

    $response = array();

    if (isset($_GET['action'])) {
        switch ($_GET['action']) {
            case 'login':
                if (checkParamaters(array('username', 'pass'))) {
                    $username = $_POST['username'];
                    $pass = $_POST['pass'];

                    $stmt = $conn->prepare("SELECT id, username FROM tb_admin WHERE username = ? AND pass = ?");
                    $stmt->bind_param("ss", $username, $pass);

                    $stmt->execute();

                    $stmt->store_result();

                    if ($stmt->num_rows > 0) {
                        $stmt->bind_result($id, $username);
                        $stmt->fetch();

                        $admin = array(
                            'id' => $id,
                            'username' => $username
                        );

                        $response['error'] = false;
                        $response['message'] = 'Login sukses';
                        $response['admin'] = $admin;
                    } else {
                        $response['error'] = true;
                        $response['message'] = 'Invalid username or password';
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = 'Required paramaters are not available';
                }
                
            break;
            case 'insert':
                if (checkParamaters(array('nim', 'sikap', 'keaktifan', 'tugas'))) {
                    $nim = $_POST['nim'];
                    $sikap = $_POST['sikap'];
                    $keaktifan = $_POST['keaktifan'];
                    $tugas = $_POST['tugas'];

                    $stmt = $conn->prepare("SELECT id, nim FROM tb_mahasiswa WHERE nim = ?");
                    $stmt->bind_param("s", $nim);
                    $stmt->execute();
                    $stmt->bind_result($id, $nim);
                    $stmt->store_result();

                    if ($stmt->num_rows == 0) {
                        $stmt = $conn->prepare("INSERT INTO tb_mahasiswa (nim, sikap, keaktifan, tugas) VALUES (? ,? ,? ,?)");
                        $stmt->bind_param("sddd", $nim, $sikap, $keaktifan, $tugas);

                        if ($stmt->execute()) {
                            $stmt->prepare("SELECT nim, sikap, keaktifan, tugas FROM tb_mahasiswa WHERE nim = ?");
                            $stmt->bind_param("s", $nim);
                            $stmt->execute();
                            $stmt->bind_result($nim, $sikap, $keaktifan, $tugas);

                            $mahasiswa = array(
                                'nim' => $nim,
                                'sikap' => $sikap,
                                'keaktifan' => $keaktifan,
                                'tugas' => $tugas
                            );

                            $stmt->close();

                            $response['error'] = false;
                            $response['exist'] = false;
                            $response['message'] = 'Insert data sukses';
                            $response['mahasiswa'] = $mahasiswa;
                        }
                    } else {
                        $stmt->fetch();
                        $mahasiswa = array(
                            'id' => $id,
                            'nim' => $nim
                        );

                        $response['error'] = true;
                        $response['exist'] = true;
                        $response['message'] = 'NIM sudah ada';
                        $response['mahasiswa'] = $mahasiswa;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = 'Required paramaters are not available';
                }
                
            break;
            case 'edit':
                if (checkParamaters(array('nim', 'sikap', 'keaktifan', 'tugas'))) {
                    $nim = $_POST['nim'];
                    $sikap = $_POST['sikap'];
                    $keaktifan = $_POST['keaktifan'];
                    $tugas = $_POST['tugas'];

                    $stmt = $conn->prepare("SELECT id FROM tb_mahasiswa WHERE nim = ?");
                    $stmt->bind_param("s", $nim);
                    $stmt->execute();
                    $stmt->store_result();

                    if ($stmt->num_rows > 0) {
                        $stmt = $conn->prepare("UPDATE tb_mahasiswa SET sikap = ?, keaktifan = ?, tugas = ? WHERE nim = ?");
                        $stmt->bind_param("ddds", $sikap, $keaktifan, $tugas, $nim);

                        if ($stmt->execute()) {
                            $stmt= $conn->prepare("SELECT id, nim, sikap, keaktifan, tugas FROM tb_mahasiswa WHERE nim = ?");
                            $stmt->bind_param("s", $nim);
                            $stmt->execute();
                            $stmt->bind_result($id, $nim, $sikap, $keaktifan, $tugas);
                            $stmt->fetch();

                            $mahasiswa = array(
                                'id' => $id,
                                'nim' => $nim,
                                'sikap' => $sikap,
                                'keaktifan' => $keaktifan,
                                'tugas' => $tugas
                            );

                            $stmt->close();

                            $response['error'] = false;
                            $response['message'] = 'Update data sukses';
                            $response['mahasiswa'] = $mahasiswa;
                        }
                    } else {
                        $response['error'] = true;
                        $response['message'] = 'NIM tidak ditemukan';
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = 'Required paramaters are not available';
                }
                
            break;
            case 'change_password':
                if (checkParamaters(array('username', 'pass', 'new_pass'))) {
                    $username = $_POST['username'];
                    $pass = $_POST['pass'];
                    $newPass = $_POST['new_pass'];

                    $stmt = $conn->prepare("SELECT id FROM tb_admin WHERE username = ? AND pass = ?");
                    $stmt->bind_param("ss", $username, $pass);
                    $stmt->execute();
                    $stmt->store_result();
                    
                    if ($stmt->num_rows > 0) {
                        $stmt = $conn->prepare("UPDATE tb_admin SET pass = ? WHERE username = ?");
                        $stmt->bind_param("ss", $newPass, $username);

                        
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = 'Required parameters are not available';
                }
                
            break;
            default:
                $response['error'] = true;
                $response['message'] = 'Invalid Operation Called';
            break;
        }
    } else {
        $response['error'] = true;
        $response['message'] = 'Invalid action';
    }

    echo json_encode($response);

    function checkParamaters($params){
        foreach ($params as $param) {
            if (!isset($_POST[$param])) {
                return false;
            }
        }
        return true;
    }
?>
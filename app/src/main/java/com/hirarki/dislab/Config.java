package com.hirarki.dislab;

/**
 * Created by hp on 18/02/2019.
 */

public class Config {
    //Root URL
    private static final String ROOT_URL = "http://192.168.100.9:8000/dislab/Api.php?action=";

    //Login URL
    public static final String LOGIN_URL = ROOT_URL + "login";

    //URL for CRUD operation
    public static final String INSERT_URL = ROOT_URL + "insert";
    public static final String EDIT_URL = ROOT_URL + "edit";
    public static final String DELETE_URL = ROOT_URL + "delete";

    //Keys for username and password as defined in our $_POST['key'] in Api.php
    //First a key for login process
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "pass";

    //If server response is equal to this that means login is successfull
    public static final String LOGIN_SUCCESS = "success";

    //Keys for SharedPreferences
    //This would be the name of our shared preferences
    public static final String SHARED_PREF_NAME = "dislabapp";

    //This would be used to store the email of current logged in user
    public static final String USERNAME_SHARED_PREF = "username";

    //We will use this to store the boolean in sharedpreferences to track user is logged in or not
    public static final String LOGGEDIN_SHARED_PREF = "loggedin";
}

package com.hirarki.dislab;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EntriActivity extends AppCompatActivity {

    private Button backButton, entriButton;
    private ImageButton ibScan;
    private IntentIntegrator codeScan;
    private EditText editTextNim, editTextSikap, editTextKeaktifan, editTextTugas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entri);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ibScan = findViewById(R.id.buttonScan);

        //Inisialisasi edit text
        editTextNim = findViewById(R.id.editTextNIM);
        editTextSikap = findViewById(R.id.editTextSikap);
        editTextKeaktifan = findViewById(R.id.editTextKeaktifan);
        editTextTugas = findViewById(R.id.editTextTugas);

        //Inisialisasi scan object
        codeScan = new IntentIntegrator(this);

        //Inisialisasi button
        backButton = findViewById(R.id.buttonBack);
        entriButton = findViewById(R.id.buttonEntri);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EntriActivity.this, MenuActivity.class));
            }
        });

        ibScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                codeScan.initiateScan();
            }
        });

        entriButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                entriNilai();
            }
        });
    }

    //Getting the scan results
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null){
            //if scan has nothing result
            if (result.getContents() == null){
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_SHORT).show();
            } else {
                //Set Value to edit text nim
                editTextNim.setText(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void entriNilai(){
        final String nim = editTextNim.getText().toString().trim();
        final String sikap = editTextSikap.getText().toString();
        final String keaktifan = editTextKeaktifan.getText().toString();
        final String tugas = editTextTugas.getText().toString() ;

        if (TextUtils.isEmpty(nim)){
            editTextNim.setError("Scan NIM required");
            editTextNim.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(String.valueOf(sikap))) {
            editTextSikap.setError("Sikap value required");
            editTextSikap.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(String.valueOf(keaktifan))) {
            editTextKeaktifan.setError("Keaktifan value required");
            editTextKeaktifan.requestFocus();
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.INSERT_URL,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject obj = new JSONObject(response);

                        if (!obj.getBoolean("error")) {
                            //Jika tidak ada error, maka operasi insert sukses
                            Toast.makeText(getApplicationContext(),
                                    obj.getString("message"), Toast.LENGTH_SHORT).show();

                            reset();
                        } else if (obj.getBoolean("error") && obj.getBoolean("exist")) {
                            //Jika nim sudah ada lakukan operasi edit
                            editNilai(nim, sikap, keaktifan, tugas);
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    obj.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(EntriActivity.this, error.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Posting parameters to url
                Map<String, String> params = new HashMap<>();
                params.put("nim", nim);
                params.put("sikap", String.valueOf(sikap));
                params.put("keaktifan", String.valueOf(keaktifan));
                params.put("tugas", String.valueOf(tugas));
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void editNilai(final String nim, final String sikap, final String keaktifan, final String tugas){
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.EDIT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if (!obj.getBoolean("error")) {
                                Toast.makeText(EntriActivity.this,
                                        obj.getString("message"), Toast.LENGTH_SHORT).show();

                                reset();
                            } else {
                                Toast.makeText(EntriActivity.this,
                                        obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EntriActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Posting parameter to url
                Map<String, String> params = new HashMap<>();
                params.put("nim", nim);
                params.put("sikap", sikap);
                params.put("keaktifan", keaktifan);
                params.put("tugas", tugas);
                return params;
            }
        };

        //Inisialisasi alert dialog / pesan peringatan
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("NIM sudah ada! Apakah mau merubah data ?");
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Jika menekan "Yes" maka akan lakukan operasi edit disini
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        //Memanggil objek alertdialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void reset(){
        editTextNim.setText("");
        editTextSikap.setText("");
        editTextKeaktifan.setText("");
        editTextTugas.setText("");
    }
}

package com.hirarki.dislab;

/**
 * Created by hp on 25/02/2019.
 */

public class Mahasiswa {
    private int id, pertemuan, keaktifan, sikap, tugas;
    private String nim;

    public Mahasiswa(int id, int pertemuan, int keaktifan, int sikap, int tugas, String nim) {
        this.id = id;
        this.pertemuan = pertemuan;
        this.keaktifan = keaktifan;
        this.sikap = sikap;
        this.tugas = tugas;
        this.nim = nim;
    }

    public int getId() {
        return id;
    }

    public int getPertemuan() {
        return pertemuan;
    }

    public int getKeaktifan() {
        return keaktifan;
    }

    public int getSikap() {
        return sikap;
    }

    public int getTugas() {
        return tugas;
    }

    public String getNim() {
        return nim;
    }
}
